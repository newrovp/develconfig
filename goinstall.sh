#!/bin/sh

noUbuntu=`uname -a | grep -i -e ubuntu -e microsoft`
VI=`which vi`
if [ "$noUbuntu" != "" ]; then
    wget https://gist.githubusercontent.com/kostaz/6e0cf1eee35a34cd6589ec15b58e682c/raw/8491560fd53c98ee85a2f9916000167190113b52/install-ripgrep-on-ubuntu.sh
    chmod 755 install-ripgrep-on-ubuntu.sh
    $SUDO ./install-ripgrep-on-ubuntu.sh
fi
go get -u github.com/jstemmer/gotags

echo "Git & go Hook 설치"
$SUDO wget https://gist.githubusercontent.com/newro/2e87f37a88e406521fc03825669d2d47/raw/458148a8b4dfbd36c0a958b63451be7c3bf92b23/gistfile1.txt -O /usr/share/git-core/templates/hooks/commit-msg
$SUDO wget https://gist.githubusercontent.com/newro/2e87f37a88e406521fc03825669d2d47/raw/07385e1f8a7e707ac7e0f4fbe5ccefed9e8f46de/gotags_hook -O /usr/share/git-core/templates/hooks/gotags
$SUDO wget https://gist.githubusercontent.com/newro/2e87f37a88e406521fc03825669d2d47/raw/43e1b1afa0578aa0751b672b11faf3e42c5d4a99/post-commit-hook -O /usr/share/git-core/templates/hooks/post-commit
$SUDO chmod 755 /usr/share/git-core/templates/hooks/commit-msg
$SUDO chmod 755 /usr/share/git-core/templates/hooks/post-commit
$SUDO chmod 755 /usr/share/git-core/templates/hooks/gotags
$SUDO cp -fp /usr/share/git-core/templates/hooks/post-commit /usr/share/git-core/templates/hooks/post-merge
$SUDO cp -fp /usr/share/git-core/templates/hooks/post-commit /usr/share/git-core/templates/hooks/post-checkout

